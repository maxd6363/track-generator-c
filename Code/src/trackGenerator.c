#include "trackGenerator.h"

#include <stdlib.h>
#include <stdio.h>

#include "coord.h"
#include "track.h"


void generateTracks(char * baseFilename, int numberOfTracks,int rows, int cols, float acceptable, float changeDirection, float percentFill){

	track_t track = {NULL, rows, cols};
	coord_t start = {rows / 2, cols / 2};
	coord_t finish;
    char filename[MAX_SIZE_FILENAME];
    int count = 0;

	trackInit(&track);
	while(count != numberOfTracks) {
		trackReset(&track);
		trackWalk(&track, start, changeDirection, percentFill, &finish);
		if (isTrackAcceptable(track, acceptable)) {
			count++;
			trackLoopBack(&track, start, finish);
			trackPolish(&track);
			trackConvertToTilesType(&track);
			track.data[start.x-1][start.y] = VERTICAL_START;
            sprintf(filename, "%s%d.dat", baseFilename, count);
			trackToFile(track, filename);
		}
	}

	trackFree(&track);


}