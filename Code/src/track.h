#ifndef TRACK_H
#define TRACK_H

#include "bool.h"
#include "coord.h"

typedef struct {
	int **data;
	int rows;
	int cols;
} track_t;

typedef enum { NONE,
			   VERTICAL,
			   HORIZONTAL,
			   CORNER_D_R,
			   CORNER_D_L,
			   CORNER_U_R,
			   CORNER_U_L,
			   CROSS_SECTION,
			   VERTICAL_START } tile_t;

void trackInit(track_t *track);
void trackPrint(track_t track, bool complexGeometry);
void trackToFile(track_t track, char * filename);
void trackFree(track_t *track);
void trackReset(track_t *track);
void trackWalk(track_t *track, coord_t start, float chanceChangeDirection, float fillSatisfied, coord_t *finish);
void trackLoopBack(track_t *track, coord_t start, coord_t finish);
void trackConvertToTilesType(track_t *track);
void trackPolish(track_t *track);


float trackPercentFill(track_t track);
direction_t randDirection(direction_t current, float percent);
bool isTrackAcceptable(track_t track, float rate);
bool isMaskCorrect(track_t track, int x, int y, int mask);
bool getBit(int number, int position);
void drawSimpleTile(int value);
void drawComplexTile(tile_t value);



#endif