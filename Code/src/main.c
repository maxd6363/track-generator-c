#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "coord.h"
#include "track.h"
#include "trackGenerator.h"

#define ACCEPTABLE_RATE 0.25
#define PERCENT_CHANGE_DIRECTION 0.15
#define PERCENT_FILL 0.06


void usage(void){
	printf("./track [numberOfTracks] [rows] [cols] [percentAcceptable] [percentChangeDirection] [percentTrackFill]\n\n");
}


int main(int argc, char **argv) {
	srand(time(NULL));
	if(argc == 7){
		generateTracks("track",atoi(argv[1]),atoi(argv[2]),atoi(argv[3]),atof(argv[4]),atof(argv[5]),atof(argv[6]));
	}
	else if(argc == 4){
		generateTracks("TrackGenerator/track",atoi(argv[1]),atoi(argv[2]),atoi(argv[3]),ACCEPTABLE_RATE,PERCENT_CHANGE_DIRECTION,PERCENT_FILL);
	}
	else{
		fprintf(stderr,"Wrong command line !\n\n");
		usage();
		return 1;
	}

	return 0;
}
