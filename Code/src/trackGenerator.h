#ifndef TRACK_GENERATOR_H
#define TRACK_GENERATOR_H

#define MAX_SIZE_FILENAME 256


void generateTracks(char * baseFilename, int numberOfTracks, int rows, int cols, float acceptable, float changeDirection, float percentFill);


#endif