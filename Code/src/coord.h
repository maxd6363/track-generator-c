#ifndef COORD_F
#define COORD_F

typedef struct {
    int x;
    int y;
}coord_t;


typedef enum {DOWN,UP,LEFT,RIGHT}direction_t;


void directionPrint(direction_t dir);
void directionToCoord(direction_t dir, coord_t *coord, int rows, int cols);

#endif