#include "coord.h"

#include <stdio.h>

void directionPrint(direction_t dir) {
	switch (dir) {
	case UP:
		printf("UP\n");
		break;
	case DOWN:
		printf("DOWN\n");
		break;
	case LEFT:
		printf("LEFT\n");
		break;
	case RIGHT:
		printf("RIGHT\n");
		break;
	}
}

void directionToCoord(direction_t dir, coord_t *coord, int rows, int cols) {
	switch (dir) {
	case UP:
		if (coord->y > 1) {
			(coord->y)--;
		}
		break;
	case DOWN:
		if (coord->y < rows - 2) {
			(coord->y)++;
		}
		break;
	case LEFT:
		if (coord->x > 1) {
			(coord->x)--;
		}
		break;
	case RIGHT:
		if (coord->x < cols - 2) {
			(coord->x)++;
		}
		break;
	}
}