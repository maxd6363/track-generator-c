#include "tests.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "coord.h"
#include "track.h"
#include "trackGenerator.h"


#define ACCEPTABLE_RATE 0.25
#define PERCENT_CHANGE_DIRECTION 0.15
#define PERCENT_FILL 0.06


void trackStats(void) {
	track_t track = {NULL, 75, 75};
	coord_t start = {50, 50};
	coord_t finish;
	int totalTry = 1000;
	int totalSuccess = 0;

	trackInit(&track);
	for (int i = 0; i < totalTry; i++) {
		trackWalk(&track, start, PERCENT_CHANGE_DIRECTION, PERCENT_FILL, &finish);
		if (isTrackAcceptable(track, ACCEPTABLE_RATE)) {
			totalSuccess++;
		}
		trackReset(&track);
	}
	printf("Rate : %.2f%%\n", ((float)totalSuccess / totalTry) * 100);

	trackFree(&track);
}

void trackVisual(void) {

	track_t track = {NULL, 25, 25};
	coord_t start = {15, 15};
	coord_t finish;
	int totalTry = 1;

	trackInit(&track);
	for (int i = 0; i < totalTry; i++) {
		trackWalk(&track, start, PERCENT_CHANGE_DIRECTION, PERCENT_FILL, &finish);
		if (isTrackAcceptable(track, ACCEPTABLE_RATE)) {
			trackLoopBack(&track, start, finish);
			trackPolish(&track);
			trackConvertToTilesType(&track);
			track.data[start.x][start.y] = VERTICAL_START;
			trackPrint(track, false);

			usleep(1000000);
		}
		trackReset(&track);
	}

	trackFree(&track);
}

void trackFile(int rows, int cols) {

	track_t track = {NULL, rows, cols};
	coord_t start = {rows / 2, cols / 2};
	coord_t finish;
	bool success = false;

	trackInit(&track);
	while(!success) {
		trackReset(&track);
		trackWalk(&track, start, PERCENT_CHANGE_DIRECTION, PERCENT_FILL, &finish);
		if (isTrackAcceptable(track, ACCEPTABLE_RATE)) {
			success = true;
			trackLoopBack(&track, start, finish);
			trackPolish(&track);
			trackConvertToTilesType(&track);
			track.data[start.x-1][start.y] = VERTICAL_START;
			trackPrint(track, true);
			trackToFile(track,"track.dat");
		}
	}

	trackFree(&track);
}
