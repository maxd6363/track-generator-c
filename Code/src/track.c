#include "track.h"

#include <stdio.h>
#include <stdlib.h>

void trackInit(track_t *track) {
	track->data = (int **)malloc(track->rows * sizeof(int *));
	for (int i = 0; i < track->rows; i++) {
		track->data[i] = (int *)malloc(track->cols * sizeof(int));
	}
}

void trackPrint(track_t track, bool complexGeometry) {
	system("clear");
	for (int i = 0; i < track.rows; i++) {
		for (int j = 0; j < track.cols; j++) {
			if (!complexGeometry)
				drawSimpleTile(track.data[i][j]);
			else
				drawComplexTile(track.data[i][j]);
		}
		printf("\n");
	}
}

void trackToFile(track_t track, char *filename) {
	FILE *flot;
	flot = fopen(filename, "w");
	if (flot == NULL)
		return;
	fprintf(flot, "%d %d\n", track.rows, track.cols);
	for (int i = 0; i < track.rows; i++) {
		for (int j = 0; j < track.cols; j++) {
			fprintf(flot, "%d ", track.data[i][j]);
		}
		fprintf(flot, "\n");
	}
	fclose(flot);
}

void trackFree(track_t *track) {
	track->cols = 0;
	track->cols = 0;
	free(track->data);
}

void trackReset(track_t *track) {
	for (int i = 0; i < track->rows; i++) {
		for (int j = 0; j < track->cols; j++) {
			track->data[i][j] = 0;
		}
	}
}

void trackWalk(track_t *track, coord_t start, float chanceChangeDirection, float fillSatisfied, coord_t *finish) {
	direction_t currentDirection = UP;
	coord_t currentCoord = start;
	float currentFill = 0.0f;
	int iteration = 0;

	track->data[currentCoord.y][currentCoord.x] = VERTICAL;
	directionToCoord(currentDirection, &currentCoord, track->rows, track->cols);
	track->data[currentCoord.y][currentCoord.x] = VERTICAL;
	directionToCoord(currentDirection, &currentCoord, track->rows, track->cols);
	
	while (currentFill < fillSatisfied) {
		iteration++;
		track->data[currentCoord.y][currentCoord.x] = 1;
		directionToCoord(currentDirection, &currentCoord, track->rows, track->cols);
		if (iteration % 4 == 3) {
			currentDirection = randDirection(currentDirection, chanceChangeDirection);
		}
		currentFill = trackPercentFill(*track);
	}

	*finish = currentCoord;
}

void trackLoopBack(track_t *track, coord_t start, coord_t finish) {
	coord_t currentCoord = finish;
	coord_t targetCoord = start;
	direction_t direction = UP;

	while (currentCoord.x != targetCoord.x || currentCoord.y != targetCoord.y) {
		track->data[currentCoord.y][currentCoord.x] = 1;
		if (currentCoord.x < targetCoord.x) {
			direction = RIGHT;
		}
		if (currentCoord.x > targetCoord.x) {
			direction = LEFT;
		}
		if (currentCoord.y < targetCoord.y) {
			direction = DOWN;
		}
		if (currentCoord.y > targetCoord.y) {
			direction = UP;
		}
		directionToCoord(direction, &currentCoord, track->rows, track->cols);
	}
}

void trackConvertToTilesType(track_t *track) {
	for (int i = 1; i < track->rows - 1; i++) {
		for (int j = 1; j < track->cols - 1; j++) {
			if (isMaskCorrect(*track, i, j, 0b01000010))
				track->data[i][j] = VERTICAL;
			if (isMaskCorrect(*track, i, j, 0b00011000))
				track->data[i][j] = HORIZONTAL;
			if (isMaskCorrect(*track, i, j, 0b00001010))
				track->data[i][j] = CORNER_U_L;
			if (isMaskCorrect(*track, i, j, 0b00010010))
				track->data[i][j] = CORNER_U_R;
			if (isMaskCorrect(*track, i, j, 0b01001000))
				track->data[i][j] = CORNER_D_L;
			if (isMaskCorrect(*track, i, j, 0b01010000))
				track->data[i][j] = CORNER_D_R;
			if (isMaskCorrect(*track, i, j, 0b01011010))
				track->data[i][j] = CROSS_SECTION;
		}
	}
}

void trackPolish(track_t *track) {
	int totalInvalid = -1;
	while (totalInvalid != 0) {
		totalInvalid = 0;
		for (int i = 1; i < track->rows - 1; i++) {
			for (int j = 1; j < track->cols - 1; j++) {
				if (track->data[i][j] != 0) {
					if (track->data[i - 1][j] + track->data[i][j - 1] + track->data[i][j + 1] + track->data[i + 1][j] == 1) {
						totalInvalid++;
						track->data[i][j] = 0;
					}
				}
			}
		}
	}
}

float trackPercentFill(track_t track) {
	int total = 0;
	for (int i = 0; i < track.rows; i++) {
		for (int j = 0; j < track.cols; j++) {
			if (track.data[i][j] == 1) {
				total++;
			}
		}
	}
	return (float)total / (track.cols * track.rows);
}

direction_t randDirection(direction_t current, float percent) {
	int randNumber = rand() % 100;
	if (randNumber > percent * 100)
		return current;

	randNumber = rand() % 100;
	if (randNumber < 25 && current != UP) {
		return DOWN;
	}
	if (randNumber < 50 && current != DOWN) {
		return UP;
	}
	if (randNumber < 75 && current != RIGHT) {
		return LEFT;
	}
	if (randNumber < 100 && current != LEFT) {
		return RIGHT;
	}
	return current;
}

bool isTrackAcceptable(track_t track, float rate) {
	int numberOfInvalidTile = 0;
	int numberOfTile = 0;

	for (int i = 1; i < track.rows - 1; i++) {
		for (int j = 1; j < track.cols - 1; j++) {
			if (track.data[i][j] != 0) {
				numberOfTile++;
				if (track.data[i - 1][j - 1] + track.data[i - 1][j] + track.data[i - 1][j + 1] + track.data[i][j - 1] + track.data[i][j + 1] + track.data[i + 1][j - 1] + track.data[i + 1][j] + track.data[i + 1][j + 1] > 2) {
					numberOfInvalidTile++;
				}
			}
		}
	}
	float percentValid = (((float)numberOfInvalidTile) / numberOfTile);
	return percentValid < rate;
}

bool isMaskCorrect(track_t track, int x, int y, int mask) {
	int countBitMask = 0;
	if (track.data[x][y] == 0)
		return false;

	for (int i = -1; i < 2; i++) {
		for (int j = -1; j < 2; j++) {
			if (i == 0 && j == 0)
				continue; // center position
			if (getBit(mask, countBitMask)) {
				if (x + i < 0 || x + i >= track.cols || y + j < 0 || y + j >= track.cols) {
					countBitMask++;
					continue;
				}
				if (track.data[x + i][y + j] == 0)
					return false;
			}
			countBitMask++;
		}
	}
	return true;
}

bool getBit(int number, int position) {
	return (((number >> position) & 1) == 1);
}

void drawSimpleTile(int value) {
	if (value != 0)
		printf("██");
	else
		printf("░░");
}

void drawComplexTile(tile_t value) {
	switch (value) {
	case NONE:
		printf(" ");
		break;
	case VERTICAL:
		printf("║");
		break;
	case VERTICAL_START:
		printf("S");
		break;
	case HORIZONTAL:
		printf("═");
		break;
	case CORNER_D_R:
		printf("╔");
		break;
	case CORNER_D_L:
		printf("╗");
		break;
	case CORNER_U_R:
		printf("╚");
		break;
	case CORNER_U_L:
		printf("╝");
		break;
	case CROSS_SECTION:
		printf("╬");
		break;
	}
}
